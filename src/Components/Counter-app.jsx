import React, { useState } from "react";
import "./Counter-app.css";
import CartItem from "./CartItem";
import RestroreCartValue from "./RestroreCartValue";
let cartItems = [
  { itemNumber: 0, isZero: true },
  { itemNumber: 0, isZero: true },
  { itemNumber: 0, isZero: true },
  { itemNumber: 0, isZero: true },
]
function CounterApp() {
  const [cartList, updateCart] = useState([...cartItems]);
  const [itemCount, updateCount] = useState(0);

  function increseCartCount(cartList){
      let count = cartList.filter((item)=>{
          return item.itemNumber>0;
      });
      updateCount(count.length);
  }

function increseCount(currItem) {
  updateCart(
    cartList.map((item) => {
      if (currItem == item) {
        item.itemNumber++;
        item.isZero = false;
      }
      return item;
    })
  );
  increseCartCount(cartList);
}
function decreseCount(currItem) {
  console.log('clicked');
  if (currItem.itemNumber > 0){
    updateCart(
      cartList.map((item) => {
        if (currItem == item) {
          item.itemNumber--;
        }
        return item;
      })
    );
  }
  if (currItem.itemNumber == 0) {
    currItem.isZero = true;
    if(itemCount >0){
    updateCount(itemCount-1);
    }
    console.log("its zero");
  }
}
function deleteItem(currItem) {
  updateCart(
    cartList.filter((item) => {
      return item != currItem;
    })
  );
  if(currItem.itemNumber > 0){
  updateCount((oldValue)=>{
      if(oldValue > 0){
        oldValue-=1;
      }
      console.log(oldValue);
      return oldValue;
  });
}
}


  function makeAllZero(){
    updateCart(
      cartList.map((item)=>{
        item.itemNumber = 0;
        return item;
      })
    )
    updateCount(0);
}


function getAllBack(){
    if(cartList.length == 0){
      updateCart(cartItems.map((item)=>{
        item.itemNumber = 0;
        return item;
      }))
    }
}
  
  return (
    <div className="counter-container">
      <div className="cart-items">
        <i className="fa fa-shopping-cart fa-lg m-2" aria-hidden="true"></i>
        <h3 id="item-count">{itemCount}</h3>
        <h3>Items</h3>
      </div>
      <RestroreCartValue cartList={cartList} makeAllZero={makeAllZero} getAllBack={getAllBack} />
      <CartItem cartList={cartList} increseCount={increseCount} decreseCount={decreseCount} deleteItem={deleteItem}/>
    </div>
  );
}

export default CounterApp;