import React from 'react'

function RestroreCartValue({makeAllZero, getAllBack} ) {
  return (
    <div className="recycle-reset">
        <button className="refresh-button" onClick={makeAllZero} > <i className="fa fa-refresh fa-lg m-2" aria-hidden="true" ></i> </button>
        <button className="recycle-button" onClick={getAllBack}> <i className="fa fa-recycle fa-lg m-2" aria-hidden="true"></i> </button>
    </div>
  );
}
export default RestroreCartValue