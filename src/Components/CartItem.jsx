import React from 'react'

const CartItem = ({cartList , increseCount , decreseCount, deleteItem}) => {

  return (
    <div>
        {cartList.map((item) => {
          return (
            <div className="cart-items">
              {item.itemNumber == 0 ? (
                <p className="zero">Zero </p>
              ) : (
                <p className="not-zero">{item.itemNumber}</p>
              )}
              <button
                className="plus-button"
                onClick={() => {
                    increseCount(item);
                }}
              >
                <i className="fa-solid fa-circle-plus fa-lg m-2"></i>
              </button>
              <button
                className="minus-button"
                disabled = {item.isZero}
                onClick={() => {
                  decreseCount(item);
                }}
              >
                <i className="fa-solid fa-circle-minus fa-lg m-2"></i>
              </button>
              <button
                className="trash-button"
                onClick={() => {
                  deleteItem(item);
                }}
              >
                <i className="fa-solid fa-trash-can fa-lg m-2"></i>
              </button>
            </div>
          );
        })}
      </div>
  )
}

export default CartItem;
